#version 330 core

layout(location = 0) in vec3 attr_position;
layout(location = 2) in vec2 attr_texCoords;

out V2F{
	vec2 uv;		
}v2f;

uniform mat4 MVP;

void main() {
	gl_Position = MVP * vec4(attr_position, 1);
	
	v2f.uv = attr_texCoords;
}