#version 330 core

out vec4 out_color;

in V2F{
	vec2 uv;	
}v2f;

uniform sampler2D texture_diffuse;

uniform vec4 color;

void main(){
	vec4 tex_rgba = texture(texture_diffuse, v2f.uv) * color;
	      
	out_color = tex_rgba;
}