#version 330 core

//! #include "../../../UniformBlock/ub_camera.glsl"
//! #include "../../../UniformBlock/ub_light.glsl"

//inc "UniformBlock/ub_camera.glsl"
//inc "UniformBlock/ub_light.glsl"

out vec4 out_color;

in V2F{
	vec2 uv;
	vec3 position_world;	
	vec3 normal_world;				
	vec4 position_light_directional[16];
}v2f;

uniform sampler2D texture_diffuse;
uniform sampler2D texture_normal;
uniform sampler2D texture_specular;

uniform float alphaCutout;

// alpha cutout / inst / no specular
void main(){				
	vec4 tex_rgba = texture(texture_diffuse, v2f.uv);

	if(tex_rgba.a < alphaCutout){
		discard;
	}

	vec3 albedo = tex_rgba.rgb;

	vec3 lighting = albedo * ambient.xyz;
	vec3 viewDir = normalize(cameraPosition.xyz - v2f.position_world);

	//Directional Lights
	for(int loop = 0; loop < lightCountDirectional; loop++){
		//diffuse
		vec3 lightDir = -directionalLight[loop].direction.xyz;
		vec3 diffuse =  max(dot(v2f.normal_world, lightDir), 0.0) * albedo * directionalLight[loop].color.xyz;		   

		lighting += diffuse;
	}

	
	//Point Lights
	for(int loop = 0; loop < lightCountPoint; loop++){
		//diffuse
		vec3 lightDir = normalize(pointLight[loop].position.xyz - v2f.position_world);
		vec3 diffuse =  max(dot(v2f.normal_world, lightDir), 0.0) * albedo * pointLight[loop].color.xyz;		   

		// attenuation
		float dist = length(pointLight[loop].position.xyz - v2f.position_world);
		float attenuation = clamp(1.0 - dist * dist / (pointLight[loop].range * pointLight[loop].range), 0.0, 1.0);	

		lighting += diffuse * attenuation;
	}
	        
	out_color = vec4(lighting, 1.0f);
}