#version 330 core

//! #include "../UniformBlock/ub_camera.glsl"
//! #include "../UniformBlock/ub_light.glsl"

//inc "UniformBlock/ub_camera.glsl"
//inc "UniformBlock/ub_light.glsl"

//DiffuseTexture + NormalTexture + SpecularTexture

layout(location = 0) in vec3 attr_position;
layout(location = 1) in vec3 attr_normal;
layout(location = 2) in vec2 attr_texCoords;
layout(location = 4) in vec3 attr_tangent;
layout(location = 5) in vec3 attr_bitangent;

out V2F{
	vec2 uv;
	vec3 position_world;	
	mat3 tbn;
	vec4 position_light_directional[16];
}v2f;

uniform mat4 MVP;
uniform mat4 M;

void main() {
	gl_Position = MVP * vec4(attr_position, 1);
	
	v2f.uv = attr_texCoords;

	v2f.position_world = (M * vec4(attr_position, 1)).xyz;		
	mat3 invTransM = transpose(inverse(mat3(M)));
	vec3 tangent = normalize(vec3(invTransM * attr_tangent));	
	vec3 normal = normalize(vec3(invTransM * attr_normal));	
	vec3 bitangent = normalize(cross(normal, tangent));

	v2f.tbn = mat3(tangent, bitangent, normal);
	
	for(int loop = 0; loop < lightCountDirectional; loop++){
		v2f.position_light_directional[loop] = directionalLight[loop].lightVP * vec4(v2f.position_world, 1);					
	}													
}