#version 330 core

//! #include "../UniformBlock/ub_camera.glsl"
//! #include "../UniformBlock/ub_light.glsl"

//inc "UniformBlock/ub_camera.glsl"
//inc "UniformBlock/ub_light.glsl"

out vec4 out_color;

in V2F{
	vec2 uv;
	vec3 position_world;	
	mat3 tbn;
	vec4 position_light_directional[16];
}v2f;


uniform sampler2D texture_diffuse;
uniform sampler2D texture_normal;

uniform float shininess;

uniform sampler2D shadowMap;

float calcShadowMap(int idx) {		
    vec3 projectedEyeDir = v2f.position_light_directional[idx].xyz/v2f.position_light_directional[idx].w;

    vec2 textureCoordinates = projectedEyeDir.xy * 0.5f + 0.5f;

    const float bias = 0.005;
    float depthValue = texture( shadowMap, textureCoordinates ).r + bias;  
	return min(projectedEyeDir.z * 0.5 + 0.5, 1) <= depthValue ? 1 : 0;
}

void main(){				
	vec3 albedo = texture(texture_diffuse, v2f.uv).rgb;
	vec3 normal = texture(texture_normal, v2f.uv).rgb;
	normal = normal * 2.0 - 1.0;
	normal = normalize(v2f.tbn * normal);

	vec3 lighting = albedo * ambient.xyz;	
	vec3 viewDir = normalize(cameraPosition.xyz - v2f.position_world);

	//Directional Lights
	for(int loop = 0; loop < lightCountDirectional; loop++){
		//diffuse
		vec3 lightDir = -directionalLight[loop].direction.xyz;
		vec3 diffuse =  max(dot(normal, lightDir), 0.0) * albedo * directionalLight[loop].color.xyz;// * vec3(1, 0, 0);		   		

		// specular		
		vec3 halfwayDir = normalize(lightDir + viewDir);
		float spec = pow(max(dot(normal, halfwayDir), 0.0), shininess);
		vec3 specular = directionalLight[loop].color.xyz * spec;

		lighting += (diffuse + specular) * calcShadowMap(loop);
	}

	
	//Point Lights
	for(int loop = 0; loop < lightCountPoint; loop++){
		//diffuse
		vec3 lightDir = normalize(pointLight[loop].position.xyz - v2f.position_world);
		vec3 diffuse =  max(dot(normal, lightDir), 0.0) * albedo * pointLight[loop].color.xyz;		   

		// attenuation	
		float dist = length(pointLight[loop].position.xyz - v2f.position_world);
		float attenuation = clamp(1.0 - (dist * dist / (pointLight[loop].range * pointLight[loop].range)), 0.0, 1.0);

		lighting += diffuse * attenuation;
	}
	        
	out_color = vec4(lighting, 1.0f);
}