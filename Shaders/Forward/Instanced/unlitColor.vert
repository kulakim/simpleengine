#version 330 core

//! #include "../../UniformBlock/ub_camera.glsl"

//inc "UniformBlock/ub_camera.glsl"


layout(location = 0) in vec3 attr_position;
layout(location = 4) in mat4 attr_matModel;

void main() {
	gl_Position = VP * attr_matModel * vec4(attr_position, 1);	
}