#pragma once

class Scene;

class SimpleEngine {
public:
	~SimpleEngine();
	static bool Initialize(int width, int height, const char* name);
	static bool LoadScene(Scene* scene);
	static void Begin();
};
