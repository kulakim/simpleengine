#pragma once

#include <Bases/BasesBundle.h>
#include <Imgui/imgui.h>

class MoveCamera : public BaseScript {
private:
	float moveSpeed;
	float sensitivity;
	float verticalAngle;
	float horizontalAngle;

	ImGuiIO* io;

public:
	MoveCamera();
	~MoveCamera();
	virtual void OnUpdate() override;
	virtual void OnStart() override;
};
