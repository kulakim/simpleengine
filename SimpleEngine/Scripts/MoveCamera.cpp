#include "MoveCamera.h"

#include <gl\glew.h>
#include <GameWindow.h>

#include <Bases/BasesBundle.h>

MoveCamera::MoveCamera() {
}

MoveCamera::~MoveCamera() {
}

void MoveCamera::OnUpdate() {
	if (GameWindow::IsFocused()) {
		if (io->MouseDown[1]) {
			GameWindow::EnableCursor(false);

			horizontalAngle -= sensitivity * io->MouseDelta.x;
			verticalAngle -= sensitivity * io->MouseDelta.y;

			if (io->KeysDown[GLFW_KEY_W]) {
				transform->AddPosition(transform->GetForward() * io->DeltaTime * moveSpeed);
			}
			if (io->KeysDown[GLFW_KEY_S]) {
				transform->AddPosition(-transform->GetForward() * io->DeltaTime * moveSpeed);
			}
			if (io->KeysDown[GLFW_KEY_D]) {
				transform->AddPosition(transform->GetRight() * io->DeltaTime * moveSpeed);
			}
			if (io->KeysDown[GLFW_KEY_A]) {
				transform->AddPosition(-transform->GetRight() * io->DeltaTime * moveSpeed);
			}
			if (io->KeysDown[GLFW_KEY_Q]) {
				transform->AddPosition(glm::vec3(0, 1, 0) * io->DeltaTime * moveSpeed);
			}
			if (io->KeysDown[GLFW_KEY_E]) {
				transform->AddPosition(glm::vec3(0, -1, 0) * io->DeltaTime * moveSpeed);
			}

			transform->SetForward(
				glm::vec3(
					cos(verticalAngle) * sin(horizontalAngle),
					sin(verticalAngle),
					cos(verticalAngle) * cos(horizontalAngle)
				)
			);

			transform->SetRight(
				glm::vec3(
					sin(horizontalAngle - 3.14f / 2.0f),
					0,
					cos(horizontalAngle - 3.14f / 2.0f)
				)
			);
		} else {
			GameWindow::EnableCursor(true);
		}
	}
}

void MoveCamera::OnStart() {
	io = &(ImGui::GetIO());

	horizontalAngle = 3.14f;
	verticalAngle = 0.f;
	moveSpeed = 20.0f;
	sensitivity = 0.002f;

	transform->SetForward(
		glm::vec3(
			cos(verticalAngle) * sin(horizontalAngle),
			sin(verticalAngle),
			cos(verticalAngle) * cos(horizontalAngle)
		)
	);

	transform->SetRight(
		glm::vec3(
			sin(horizontalAngle - 3.14f / 2.0f),
			0,
			cos(horizontalAngle - 3.14f / 2.0f)
		)
	);
}