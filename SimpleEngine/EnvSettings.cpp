#include "EnvSettings.h"

#include <Logger/SpLogger.h>

#include <fstream>
#include <iostream>

#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>
#include <cstdio>

using namespace rapidjson;

string EnvSettings::pathShaders = "NotInitialized";
string EnvSettings::pathResources = "NotInitialized";

bool EnvSettings::ReadPath() {
	DebugLog("Read environment path...");

	string str;
	ifstream openFile(FILENAME_SETTINGS);
	if (openFile.is_open()) {
		string line;
		while (getline(openFile, line)) {
			str += line;
		}
		openFile.close();
	} else {
		DebugError("Failed open file: " + string(FILENAME_SETTINGS));
	}

	// Default template parameter uses UTF8 and MemoryPoolAllocator.
	Document document;

	if (document.Parse(str.data()).HasParseError()) {
		return false;
	}

	//Shaders
	assert(document.HasMember(VAR_PATH_SHADERS));
	assert(document[VAR_PATH_SHADERS].IsString());
	pathShaders = document[VAR_PATH_SHADERS].GetString();
	DebugLog("\tPath for Shaders: " + pathShaders);

	//Resources
	assert(document.HasMember(VAR_PATH_RESOURCES));
	assert(document[VAR_PATH_RESOURCES].IsString());
	pathResources = document[VAR_PATH_RESOURCES].GetString();
	DebugLog("\tPath for Resources: " + pathResources);

	DebugLog("Done reading environment path");

	return true;
}

string EnvSettings::GetPathShaders() {
	return pathShaders;
}

string EnvSettings::GetPathResources() {
	return pathResources;
}