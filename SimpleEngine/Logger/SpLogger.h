#pragma once

#include <iostream>
#include <string>
#include <EngineDef.h>

#ifdef DEBUG
#define DebugLog(string) SpLogger::Log(string)
#define DebugError(string) SpLogger::Error(string)
#define DebugWarning(string) SpLogger::Warning(string)
#else
#define DebugLog(string)
#define DebugError(string)
#define DebugWarning(string)
#endif

using namespace::std;
class SpLogger {
public:
	static int logLevel;

	static void Log(string str_);
	static void Log(string str_, int logLevel_);//logLevel_ <= logLevel  ->  ���

	static void Error(string str_);
	static void Warning(string str_);

private:
	SpLogger();
	~SpLogger();
};
