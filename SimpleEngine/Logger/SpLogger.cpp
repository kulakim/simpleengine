#include "SpLogger.h"

int SpLogger::logLevel = 0;

void SpLogger::Log(string str_) {
	cout << str_ << endl;
}

void SpLogger::Log(string str_, int logLevel_) {
	if (logLevel <= logLevel_) {
		cout << str_ << endl;
	}
}

void SpLogger::Error(string str_) {
	cout << str_ << endl;
}

void SpLogger::Warning(string str_) {
	cout << str_ << endl;
}

SpLogger::SpLogger() {
}

SpLogger::~SpLogger() {
}