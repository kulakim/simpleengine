#pragma once

#define DEBUG //comment this to disable log

#pragma region Shader

#define VertexShaderFormat ".vert"
#define FragmentShaderFormat ".frag"
#define GeometryShaderFormat ".geom"

#pragma endregion

enum VsyncMode {
	VSYNC_OFF,
	VSYNC_ON
};