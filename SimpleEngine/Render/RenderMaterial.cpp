#include "RenderMaterial.h"

#include <gl/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <Render/Texture.h>
#include <Render/Renderer/MeshRenderer.h>
#include <Light/LightsBundle.h>
#include <Shaders/ShaderBundle.h>
#include <Logger/SpLogger.h>

RenderMaterial::RenderMaterial() {
}

RenderMaterial::RenderMaterial(std::string shaderPath) {
	shader = new BaseShader(shaderPath);

	DebugLog("RenderMaterial Created: " + shaderPath);
}

RenderMaterial::~RenderMaterial() {
}

void RenderMaterial::SetTexture(std::string name, Texture* texture) {
	if (mapTexture.find(name) == mapTexture.end()) {//never seen before
		BoundTexture bt;

		bt.activeTextureIndex = nextTextureIdx++;
		bt.texture = texture;

		mapTexture.insert(std::make_pair(name, bt));

		//TODO FIXME
		if (shader != NULL) {
			shader->SetInt(name, bt.activeTextureIndex);
		}
	} else {//already exists
		mapTexture[name].texture = texture;
	}
}

void RenderMaterial::SetFloat(std::string name, float value) {
	if (mapFloat.find(name) == mapFloat.end()) {//never seen before
		mapFloat.insert(std::make_pair(name, value));
	} else {//already exists
		mapFloat[name] = value;
	}

	shader->SetFloat(name, value);
}

void RenderMaterial::SetInt(std::string name, int value) {
	if (mapInt.find(name) == mapInt.end()) {//never seen before
		mapInt.insert(std::make_pair(name, value));
	} else {//already exists
		mapInt[name] = value;
	}

	shader->SetInt(name, value);
}

void RenderMaterial::SetVec2(std::string name, glm::vec2 value) {
	if (mapVec2.find(name) == mapVec2.end()) {//never seen before
		mapVec2.insert(std::make_pair(name, value));
	} else {//already exists
		mapVec2[name] = value;
	}

	shader->SetVec2(name, value);
}

void RenderMaterial::SetVec3(std::string name, glm::vec3 value) {
	if (mapVec3.find(name) == mapVec3.end()) {//never seen before
		mapVec3.insert(std::make_pair(name, value));
	} else {//already exists
		mapVec3[name] = value;
	}

	shader->SetVec3(name, value);
}

void RenderMaterial::SetVec4(std::string name, glm::vec4 value) {
	if (mapVec4.find(name) == mapVec4.end()) {//never seen before
		mapVec4.insert(std::make_pair(name, value));
	} else {//already exists
		mapVec4[name] = value;
	}

	shader->SetVec4(name, value);
}

Texture* RenderMaterial::GetTexture(std::string name) {
	return mapTexture[name].texture;
}

float RenderMaterial::GetFloat(std::string name) {
	return mapFloat[name];
}

int RenderMaterial::GetInt(std::string name) {
	return mapInt[name];
}

glm::vec2 RenderMaterial::GetVec2(std::string name) {
	return mapVec2[name];
}

glm::vec3 RenderMaterial::GetVec3(std::string name) {
	return mapVec3[name];
}

glm::vec4 RenderMaterial::GetVec4(std::string name) {
	return mapVec4[name];
}

string RenderMaterial::GetShaderPath() {
	if (shader != NULL) {
		return shader->GetFilePath();
	} else {
		return "NULL";
	}
}

/// <summary>
/// Render Setup for forwardRendering
/// called before glDraw Funcs
/// </summary>
/// <param name="renderer_"></param>
void RenderMaterial::RenderSetUp(MeshRenderer* renderer_) {
	if (shader == NULL) {
		return;
	}

	if (drawBackFace) {
		glDisable(GL_CULL_FACE);
	} else {
		glEnable(GL_CULL_FACE);
	}

	if (isTransparent) {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	} else {
		glDisable(GL_BLEND);
	}

	shader->SetMat_M(renderer_->Mmatrix());
	shader->SetMat_MVP(renderer_->MVPmatrix());

	if (renderer_->receiveShadow) {
		int dLightCount = LightManager::Inst()->directionalLights.size();
		for (int loop = 0; loop < dLightCount; loop++) {
			DirectionalLight* dl = LightManager::Inst()->directionalLights[loop];

			if (dl->castShadow) {
				glActiveTexture(GL_TEXTURE0 + ShadowMapID);
				glBindTexture(GL_TEXTURE_2D, dl->GetShadowMapData().depthMapTextureId);
			}
		}
	}

	for (auto boundTex = mapTexture.begin(); boundTex != mapTexture.end(); boundTex++) {
		int targetTexIndex = boundTex->second.activeTextureIndex;
		GLuint targetTexId = boundTex->second.texture->textureId;

		glActiveTexture(GL_TEXTURE0 + targetTexIndex);
		glBindTexture(GL_TEXTURE_2D, targetTexId);
	}
}