#include "MeshRendererInstanced.h"

#include <Shaders/BaseShader.h>
#include <FilePooler.h>
#include <Mesh/MeshModel.h>

#include <Scene/SceneIncludes.h>
#include <Bases/BasesBundle.h>
#include <Light/LightsBundle.h>
#include <Render/RenderMaterial.h>

MeshRendererInstanced::MeshRendererInstanced(RenderType renderType_) {
	renderType = renderType_;
	isInstanced = true;
}

MeshRendererInstanced::~MeshRendererInstanced() {
}

void MeshRendererInstanced::InitInstanced() {
	int childCount = transform->GetChildCount();

	if (childCount <= 0) {
		return;
	}

	std::vector<glm::mat4> vecModelMats;
	for (int loop = 0; loop < childCount; loop++) {
		vecModelMats.push_back(transform->GetChildAt(loop)->GetModelMatrix());
	}

	int meshCount = meshModel->GetMeshCount();
	for (int loop = 0; loop < meshCount; loop++) {
		Mesh* currentMesh = meshModel->GetMesh(loop);

		glBindVertexArray(currentMesh->VAO);

		glGenBuffers(1, &currentMesh->instanceVBO);
		glBindBuffer(GL_ARRAY_BUFFER, currentMesh->instanceVBO);

		glBufferData(GL_ARRAY_BUFFER, transform->GetChildCount() * sizeof(glm::mat4), &vecModelMats[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);
		glVertexAttribDivisor(4, 1);

		glEnableVertexAttribArray(4 + 1);
		glVertexAttribPointer(4 + 1,
			4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));
		glVertexAttribDivisor(4 + 1, 1);

		glEnableVertexAttribArray(4 + 2);
		glVertexAttribPointer(4 + 2,
			4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
		glVertexAttribDivisor(4 + 2, 1);

		glEnableVertexAttribArray(4 + 3);
		glVertexAttribPointer(4 + 3,
			4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));
		glVertexAttribDivisor(4 + 3, 1);
	}
}

void MeshRendererInstanced::RenderMesh() {
	for (GLuint loop = 0; loop < meshModel->GetMeshCount(); loop++) {
		Mesh* processingMesh = meshModel->GetMesh(loop);

		glBindVertexArray(processingMesh->VAO);

		glDrawElementsInstanced(
			GL_TRIANGLES,
			processingMesh->triangles.size() * 3,
			GL_UNSIGNED_INT,
			0,
			transform->GetChildCount()
		);
	}
}

void MeshRendererInstanced::Render() {
	switch (renderType) {
	case RenderType_Forward:

		for (GLuint loop = 0; loop < meshModel->GetMeshCount(); loop++) {
			Mesh* processingMesh = meshModel->GetMesh(loop);

			processingMesh->GetRenderMaterial()->RenderSetUp(this);

			glBindVertexArray(processingMesh->VAO);

			glDrawElementsInstanced(
				GL_TRIANGLES,
				processingMesh->triangles.size() * 3,
				GL_UNSIGNED_INT,
				0,
				transform->GetChildCount()
			);
		}

		break;

	case RenderType_Deferred:
		for (GLuint loop = 0; loop < meshModel->GetMeshCount(); loop++) {
			Mesh* processingMesh = meshModel->GetMesh(loop);

			GLuint diffuseId = processingMesh->GetRenderMaterial()->GetTexture("texture_diffuse")->textureId;
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, diffuseId);

			glBindVertexArray(processingMesh->VAO);
			glDrawElementsInstanced(
				GL_TRIANGLES,
				processingMesh->triangles.size() * 3,
				GL_UNSIGNED_INT,
				0,
				transform->GetChildCount()
			);
		}
		break;

	default:
		DebugError("Invalid RenderType: " + renderType);
		break;
	}
}