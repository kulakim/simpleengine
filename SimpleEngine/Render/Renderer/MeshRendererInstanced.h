#pragma once

#include <vector>
#include <Render/Renderer/MeshRenderer.h>

class Transform;
class RenderData;
class EngineObject;

class MeshRendererInstanced : public MeshRenderer {
private:

public:
	MeshRendererInstanced(RenderType);
	~MeshRendererInstanced();

	void InitInstanced();
	virtual void RenderMesh() override;
	virtual void Render() override;
};
