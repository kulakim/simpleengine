#include "MeshRenderer.h"

#include <Mesh/MeshModel.h>
#include <Light/LightsBundle.h>
#include <FilePooler.h>

#include <Scene/SceneIncludes.h>
#include <Shaders/ShaderBundle.h>
#include <Render/RenderMaterial.h>

MeshRenderer::MeshRenderer() {
}

MeshRenderer::MeshRenderer(RenderType renderType_) {
	renderType = renderType_;
}

MeshRenderer::MeshRenderer(MeshModel* meshModel_) {
	if (meshModel_ != NULL) {
		SetMeshModel(meshModel_);
	} else {
		DebugLog("MeshRenderer Init parameter NULL");
	}
}

MeshRenderer::~MeshRenderer() {
}

/// <summary>
/// Forward Rendering: Render Mesh using material
/// Deferred Rendering: Render Mesh for geometry pass. "texture_diffuse" is used for ab
/// </summary>
void MeshRenderer::Render() {
	switch (renderType) {
	case RenderType_Forward:

		for (GLuint loop = 0; loop < meshModel->GetMeshCount(); loop++) {
			Mesh* processingMesh = meshModel->GetMesh(loop);

			processingMesh->GetRenderMaterial()->RenderSetUp(this);

			glBindVertexArray(processingMesh->VAO);
			glDrawElements(
				GL_TRIANGLES,
				processingMesh->triangles.size() * 3,
				GL_UNSIGNED_INT,
				NULL
			);
		}
		break;

	case RenderType_Deferred:
		for (GLuint loop = 0; loop < meshModel->GetMeshCount(); loop++) {
			Mesh* processingMesh = meshModel->GetMesh(loop);

			GLuint diffuseId = processingMesh->GetRenderMaterial()->GetTexture("texture_diffuse")->textureId;
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, diffuseId);

			glBindVertexArray(processingMesh->VAO);
			glDrawElements(
				GL_TRIANGLES,
				processingMesh->triangles.size() * 3,
				GL_UNSIGNED_INT,
				NULL
			);
		}
		break;

	default:
		DebugError("Invalid RenderType: " + renderType);
		break;
	}
}

/// <summary>
/// Renders Mesh Only
/// glDrawElements without any inputs (uniforms, textures...)
/// </summary>
void MeshRenderer::RenderMesh() {
	for (GLuint loop = 0; loop < meshModel->GetMeshCount(); loop++) {
		Mesh* processingMesh = meshModel->GetMesh(loop);

		glBindVertexArray(processingMesh->VAO);

		glDrawElements(
			GL_TRIANGLES,
			processingMesh->GetIdxCount(),
			GL_UNSIGNED_INT,
			NULL
		);
	}
}

void MeshRenderer::SetMeshModel(MeshModel* meshModel_) {
	meshModel = meshModel_;
}

MeshModel* MeshRenderer::GetMeshModel() {
	return meshModel;
}

Mesh* MeshRenderer::GetMesh(int idx) {
	Mesh* retMesh = NULL;

	if (idx < meshModel->GetMeshCount()) {
		retMesh = meshModel->GetMesh(idx);
	}

	return retMesh;
}

bool MeshRenderer::IsInstanced() {
	return isInstanced;
}

void MeshRenderer::ComputeMatrices(Camera* camera_) {
	mvpMatrix = camera_->VPmatrix() * transform->GetModelMatrix();
}

glm::mat4 MeshRenderer::Mmatrix() {
	return transform->GetModelMatrix();
}

glm::mat4 MeshRenderer::MVPmatrix() {
	return mvpMatrix;
}

RenderType MeshRenderer::GetRenderType() {
	return renderType;
}

void MeshRenderer::SetRenderType(RenderType renderType_) {
	renderType = renderType_;
}

void MeshRenderer::OnAttachedToObject(EngineObject* obj_) {
	BaseComponent::OnAttachedToObject(obj_);

	transform = obj_->GetTransform();
	Scene::GetInstance()->AddRenderer(this);
}

void MeshRenderer::DrawInspectorImGui() {
	ImGui::Text("ReceiveShadow:  %s", receiveShadow ? "true" : "false");
	ImGui::Text("CastShadow:  %s", castShadow ? "true" : "false");
	ImGui::Text("Instancing:  %s", isInstanced ? "true" : "false");
	ImGui::Text("MeshModel:  %s", meshModel->GetFilePath().c_str());
	for (int loop = 0; loop < meshModel->GetMeshCount(); loop++) {
		ImGui::SetCursorPosX(20);
		RenderMaterial* mat = meshModel->GetMesh(loop)->GetRenderMaterial();
		if (mat != NULL) {
			ImGui::Text("[%d] %s", loop, mat->GetShaderPath().c_str());
		}
	}
}