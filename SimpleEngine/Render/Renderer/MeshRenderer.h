#pragma once

#include <Bases/BaseComponent.h>

#include <stddef.h>                           // for NULL
#include "glm/detail/type_mat.hpp"     // for mat4
#include "glm/detail/type_mat4x4.hpp"  // for tmat4x4
class EngineObject;

class Camera;
class Mesh;
class MeshModel;
class BaseLight;

enum RenderType {
	RenderType_Forward,
	RenderType_Forward_Transparent,
	RenderType_Deferred,
};

class MeshRenderer : public BaseComponent {
protected:
	RenderType renderType = RenderType_Deferred;
	MeshModel* meshModel = NULL;

	glm::mat4 mvpMatrix;
	bool isInstanced = false;
	int renderOrder = 0;

public:
	MeshRenderer();
	MeshRenderer(RenderType);
	MeshRenderer(MeshModel* meshModel_);

	virtual ~MeshRenderer();

	virtual void Render();
	virtual void RenderMesh();

	void SetMeshModel(MeshModel* meshModel_);

	MeshModel* GetMeshModel();

	/// <summary>
	/// returns meshModel->meshes->at(idx)
	/// </summary>
	/// <param name="idx"></param>
	/// <returns>meshModel->meshes->at(idx)</returns>
	Mesh* GetMesh(int idx);

	bool receiveShadow = true;
	bool castShadow = true;
	bool IsInstanced();

	void ComputeMatrices(Camera* camera_);
	glm::mat4 Mmatrix();
	glm::mat4 MVPmatrix();

	RenderType GetRenderType();
	void SetRenderType(RenderType);

	virtual void RenderShadowMap(BaseLight* light_) {}
	virtual void OnAttachedToObject(EngineObject*) override;
	virtual void DrawInspectorImGui() override;
};
