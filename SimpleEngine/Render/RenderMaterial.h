#pragma once

#include <unordered_map>
#include <string>
#include <glm/glm.hpp>

class Texture;
class BaseShader;
class MeshRenderer;

struct BoundTexture {
	Texture* texture;
	int activeTextureIndex;
};

using namespace std;

class RenderMaterial {
private:
	int nextTextureIdx = 0;

	unordered_map<string, BoundTexture> mapTexture;
	unordered_map<string, float> mapFloat;
	unordered_map<string, int> mapInt;
	unordered_map<string, glm::vec2> mapVec2;
	unordered_map<string, glm::vec3> mapVec3;
	unordered_map<string, glm::vec4> mapVec4;

protected:
	BaseShader* shader;

public:
	RenderMaterial();
	RenderMaterial(string);
	~RenderMaterial();

	void SetTexture(string, Texture*);
	void SetFloat(string, float);
	void SetInt(string, int);
	void SetVec2(string, glm::vec2);
	void SetVec3(string, glm::vec3);
	void SetVec4(string, glm::vec4);

	Texture* GetTexture(string);
	float GetFloat(string);
	int GetInt(string);
	glm::vec2 GetVec2(string);
	glm::vec3 GetVec3(string);
	glm::vec4 GetVec4(string);

	string GetShaderPath();

	void RenderSetUp(MeshRenderer*);

	bool drawBackFace = false;
	bool isTransparent = false;
};
