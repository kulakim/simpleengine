#pragma once

#include <Bases/BasesBundle.h>
#include <Imgui/imgui.h>

class ObjectHierarchyView : public BaseScript {
private:
	ImGuiIO* io;

	bool open;

	EngineObject* selectedObj = NULL;
	long selectedObjectId;

	int nodeId;

	ImGuiTreeNodeFlags baseFlags;

	void ObjectInspectorView(EngineObject* object);
	void ProcessTreeNode(vector<EngineObject*>*);
	void ProcessTreeNode(vector<Transform*>*);
	void CreateTreeNode(EngineObject* obj);

public:
	ObjectHierarchyView();
	~ObjectHierarchyView();

	virtual void OnUpdate() override;
	virtual void OnStart() override;
};
