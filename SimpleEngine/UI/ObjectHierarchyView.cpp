#include "ObjectHierarchyView.h"

#include <Scene/Scene.h>
#include <GLFW/glfw3.h>
#include <Scene/EngineObjectCollection.h>

ObjectHierarchyView::ObjectHierarchyView() {
	io = &(ImGui::GetIO());
	open = true;

	baseFlags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_SpanAvailWidth;
}

ObjectHierarchyView::~ObjectHierarchyView() {
}

void ObjectHierarchyView::OnUpdate() {
	if (ImGui::IsKeyDown(GLFW_KEY_LEFT_CONTROL) && ImGui::IsKeyPressed(GLFW_KEY_H)) {
		open = !open;
	}

	if (open == false) {
		return;
	}

	ImGui::SetNextWindowSize(ImVec2(500, 440), ImGuiCond_FirstUseEver);
	if (ImGui::Begin("Object Hierarchy", &open, ImGuiWindowFlags_None)) {
		EngineObjectCollection* engineCol = Scene::ObjectCollection();

		ImGui::BeginChild("left pane", ImVec2(200, 0), true);

		nodeId = 0;
		ProcessTreeNode(engineCol->GetRoots());

		ImGui::EndChild();

		ImGui::SameLine();

		ObjectInspectorView(selectedObj);
	}

	ImGui::End();
}

void ObjectHierarchyView::ProcessTreeNode(vector<EngineObject*>* objects) {
	for (int loop = 0; loop < objects->size(); loop++) {
		EngineObject* obj = objects->at(loop);

		CreateTreeNode(obj);
	}
}

void ObjectHierarchyView::ProcessTreeNode(vector<Transform*>* transforms) {
	for (int loop = 0; loop < transforms->size(); loop++) {
		EngineObject* obj = transforms->at(loop)->GetEngineObject();

		CreateTreeNode(obj);
	}
}

void ObjectHierarchyView::CreateTreeNode(EngineObject* obj) {
	ImGuiTreeNodeFlags nodeFlags = baseFlags;

	if (selectedObjectId == obj->GetObjectId()) {
		nodeFlags |= ImGuiTreeNodeFlags_Selected;
		selectedObj = obj;
	}

	if (obj->GetTransform()->GetChildCount() <= 0) {
		nodeFlags |= ImGuiTreeNodeFlags_Leaf;
	}

	bool nodeOpen = ImGui::TreeNodeEx((void*)(intptr_t)nodeId, nodeFlags, obj->name.c_str(), nodeId++);
	if (ImGui::IsItemClicked()) {
		selectedObjectId = obj->GetObjectId();
	}

	if (nodeOpen) {
		if (obj->GetTransform()->GetChildCount() > 0) {
			ProcessTreeNode(obj->GetTransform()->GetChildren());
		}

		ImGui::TreePop();
	}
}

void ObjectHierarchyView::OnStart() {
}

void ObjectHierarchyView::ObjectInspectorView(EngineObject* object) {
	if (object != NULL) {
		ImGui::BeginGroup();
		ImGui::BeginChild("item view", ImVec2(0, -ImGui::GetFrameHeightWithSpacing()));
		ImGui::Text(selectedObj->name.c_str());
		ImGui::Separator();

		object->GetTransform()->DrawInspectorImGui();

		ImGuiTreeNodeFlags flag;

		vector<BaseComponent*> components = object->GetComponents();
		for (int loop = 0; loop < components.size(); loop++) {
			const char* className = typeid(*components[loop]).name();
			if (ImGui::CollapsingHeader(className, ImGuiTreeNodeFlags_DefaultOpen)) {
				components[loop]->DrawInspectorImGui();
			}
		}

		ImGui::EndChild();
		ImGui::EndGroup();
	}
}