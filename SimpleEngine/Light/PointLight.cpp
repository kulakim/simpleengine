#include "PointLight.h"
#include <Bases/Transform.h>
#include <gl\glew.h>
#include <glm\glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <Light/LightManager.h>

void PointLight::UpdateUboIntensity() {
	float inten = GetIntensity();
	glBufferSubData(GL_UNIFORM_BUFFER,
		startAddrUbo,
		sizeof(float),
		&inten);
}
void PointLight::UpdateUboRange() {
	glBufferSubData(GL_UNIFORM_BUFFER,
		startAddrUbo + sizeof(float),
		sizeof(float),
		&range);
}

PointLight::PointLight() {
	lightType = LightType_Point;

	color = glm::vec3(1, 1, 1);
}

PointLight::~PointLight() {
}

void PointLight::SetRange(float range_) {
	range = range_;

	matScale = scale(mat4(1.0), vec3(range));

	modelMatrix = matTranslation * matScale;

	if (GetTransform() != NULL) {
		UpdateUboRange();
	}
}

float PointLight::GetRange() {
	return range;
}

glm::mat4 PointLight::GetModelMatrix() {
	return modelMatrix;
}

void PointLight::UpdateUbo() {
	LightManager::Inst()->BindUboLightData();

	// 0 - 4
	UpdateUboIntensity();

	// 4 - 8
	UpdateUboRange();

	//position 16 - 32
	glm::vec3 position = GetTransform()->GetPosition();
	glBufferSubData(GL_UNIFORM_BUFFER,
		startAddrUbo + sizeof(glm::vec4),
		sizeof(glm::vec4),
		&position);

	//color 32 - 48
	glBufferSubData(GL_UNIFORM_BUFFER,
		startAddrUbo + sizeof(glm::vec4) * 2,
		sizeof(glm::vec4),
		&color);
}

void PointLight::OnTransformChanged() {
	matTranslation = translate(mat4(1.0), GetTransform()->GetPosition());
	modelMatrix = matTranslation * matScale;

	UpdateUbo();
}

void PointLight::OnAttachedToObject(EngineObject* obj_) {
	BaseLight::OnAttachedToObject(obj_);

	matTranslation = translate(mat4(1.0), GetTransform()->GetPosition());
	matScale = scale(mat4(1.0), vec3(range));

	modelMatrix = matTranslation * matScale;
}