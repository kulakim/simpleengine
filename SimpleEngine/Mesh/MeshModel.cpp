#include "MeshModel.h"

#include <fstream>
#include <sstream>

#include <EnvSettings.h>
#include "../Render/Texture.h"
#include "../FilePooler.h"
#include <Render/RenderMaterial.h>
#include "MeshModelLoader.h"

MeshModel::MeshModel() {}

MeshModel::MeshModel(string path_) {
	DebugLog("Load MeshModel: " + path_);

	LoadModel(path_);
}

MeshModel::~MeshModel() {
	for (int loop = 0; loop < meshes.size(); loop++) {
		delete(meshes.at(loop));
	}
}

string MeshModel::GetFilePath() {
	return dirPath + fileName;
}

Mesh* MeshModel::GetMesh(int idx) {
	return meshes.at(idx);
}

int MeshModel::GetMeshCount() {
	return meshes.size();
}

void MeshModel::LoadModel(string path_) {
	size_t foundIdx = path_.find_last_of('/');
	dirPath = path_.substr(0, foundIdx + 1);
	fileName = path_.substr(foundIdx + 1);

	string fullFilePath = EnvSettings::GetPathResources() + path_;

	// Read file via ASSIMP
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(fullFilePath, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices | aiProcess_CalcTangentSpace);

	// Check for errors
	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) { // if is Not Zero
		cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
		return;
	}
	std::cout << "\tMesh Count: " << scene->mNumMeshes << endl;

	// Process ASSIMP's root node recursively
	ProcessNode(scene->mRootNode, scene);

	int vCount = 0;
	int tCount = 0;
	for (int loop = 0; loop < meshes.size(); loop++) {
		vCount += meshes.at(loop)->vertices.size();
		tCount += meshes.at(loop)->triangles.size();
	}

	std::cout << "\tVertex Count: " << vCount
		<< " / Triangle Count: " << tCount << std::endl;
}

void MeshModel::ProcessNode(aiNode* node, const aiScene* scene) {
	// Process all the node's meshes (if any)
	for (GLuint i = 0; i < node->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		meshes.push_back(ProcessMesh(mesh, scene));
	}

	// Then do the same for each of its children
	for (GLuint i = 0; i < node->mNumChildren; i++) {
		ProcessNode(node->mChildren[i], scene);
	}
}

Mesh* MeshModel::ProcessMesh(aiMesh* mesh, const aiScene* scene) {
	vector<Vertex> vertices;
	vector<Triangle> triangles;
	vector<Texture*> textures;

	// Walk through each of the mesh's vertices
	for (GLuint i = 0; i < mesh->mNumVertices; i++) {
		Vertex vertex;

		// positions
		if (mesh->HasPositions()) {
			vertex.position = glm::vec3(mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z);
		}

		if (mesh->HasNormals()) {
			vertex.normal = glm::vec3(mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z);;
		}

		if (mesh->HasTangentsAndBitangents()) {
			vertex.tangent = glm::vec3(mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z);
			vertex.bitangent = glm::vec3(mesh->mBitangents[i].x, mesh->mBitangents[i].y, mesh->mBitangents[i].z);
		}

		// Texture Coordinates
		if (mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.texCoords = vec;
		} else {
			vertex.texCoords = glm::vec2(0.0f, 0.0f);
		}

		vertices.push_back(vertex);
	}
	// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
	for (GLuint i = 0; i < mesh->mNumFaces; i++) {
		aiFace face = mesh->mFaces[i];
		// Retrieve all indices of the face and store them in the indices vector
		for (GLuint j = 0; j < face.mNumIndices / 3; j++) {
			Triangle tri;
			for (int loop = 0; loop < 3; loop++) {
				tri.idx[loop] = face.mIndices[j * 3 + loop];
			}

			triangles.push_back(tri);
		}
	}

	// Process materials
	RenderMaterial* renderMaterial = new RenderMaterial();
	if (mesh->mMaterialIndex >= 0) {
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

		renderMaterial->SetTexture("texture_diffuse", LoadMaterialTextures(material, aiTextureType_DIFFUSE));
		renderMaterial->SetTexture("texture_specular", LoadMaterialTextures(material, aiTextureType_SPECULAR));
		renderMaterial->SetTexture("texture_normal", LoadMaterialTextures(material, aiTextureType_NORMALS));
	}

	return new Mesh(vertices, triangles, renderMaterial);
}

Texture* MeshModel::LoadMaterialTextures(aiMaterial* aiMat_, aiTextureType aiType_) {
	std::string pathResources = EnvSettings::GetPathResources();

	if (aiMat_->GetTextureCount(aiType_) > 0) {
		aiString strPath;
		aiMat_->GetTexture(aiType_, 0, &strPath);

		string fileNameTexture = string(strPath.C_Str());

		return FilePooler::LoadTexture(pathResources + dirPath + fileNameTexture);
	} else {
		return FilePooler::LoadTexture(pathResources + string("whiteone.png"));
	}
}