#pragma once

#include <assimp\postprocess.h>
#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <string>
#include <vector>

class Mesh;
class MeshModel;
class Texture;

/// <summary>
/// MeshModel 생성을 담당합니다.
/// 3D 데이터를 File에서 읽어 메쉬모델 객체로 변환합니다.
/// </summary>
static class MeshModelLoader {
private:
	static std::string dirPath;///현재 Load중인 디렉토리 경로
	static std::string fileName;///현재 Load중인 디렉토리 경로

	static void ProcessNode(aiNode* node, const aiScene* scene, std::vector<Mesh*>* meshes);
	static Mesh* ProcessMesh(aiMesh* mesh, const aiScene* scene);
	static Texture* LoadMaterialTextures(aiMaterial* aiMat_, aiTextureType aiType_);

public:
	static MeshModel* LoadModel(std::string path);
};
