#include "Mesh.h"

#include <Shaders/BaseShader.h>
#include <Render/Texture.h>

#include <Logger/SpLogger.h>

#include <fstream>
#include <sstream>

Mesh::Mesh() {
}

Mesh::~Mesh() {
}

void Mesh::Setup() {
	if (vertices.size() <= 0)return;

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(AttrLoc_Position);
	glVertexAttribPointer(AttrLoc_Position, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);//이후 shader에서 참조할때 정의된 attribute에 따라 vbo에서 가져옴
	//0번째 attribute를 정의

	glEnableVertexAttribArray(AttrLoc_Normal);
	glVertexAttribPointer(AttrLoc_Normal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));

	glEnableVertexAttribArray(AttrLoc_TexCoord);
	glVertexAttribPointer(AttrLoc_TexCoord, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoords));

	glEnableVertexAttribArray(AttrLoc_Color);
	glVertexAttribPointer(AttrLoc_Color, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, color));

	glEnableVertexAttribArray(AttrLoc_Tangent);
	glVertexAttribPointer(AttrLoc_Tangent, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, tangent));

	glEnableVertexAttribArray(AttrLoc_Bitangent);
	glVertexAttribPointer(AttrLoc_Bitangent, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, bitangent));

	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, triangles.size() * sizeof(Triangle),
		&triangles[0], GL_STATIC_DRAW);
}

void Mesh::SetRenderMaterial(RenderMaterial* renderMaterial_) {
	renderMaterial = renderMaterial_;
}

RenderMaterial* Mesh::GetRenderMaterial() {
	return renderMaterial;
}

void Mesh::RecalculateTangents() {
	DebugLog("RecalculateTangents");

	for (int loop = 0; loop < triangles.size(); loop++) {
		int idx0 = triangles[loop].idx[0];
		int idx1 = triangles[loop].idx[1];
		int idx2 = triangles[loop].idx[2];

		glm::vec3 edge0 = vertices[idx1].position - vertices[idx0].position;
		glm::vec3 edge1 = vertices[idx2].position - vertices[idx0].position;

		glm::vec2 uv0 = vertices[idx1].texCoords - vertices[idx0].texCoords;
		glm::vec2 uv1 = vertices[idx2].texCoords - vertices[idx0].texCoords;

		float denom = 1.0f / (uv0.x * uv1.y - uv1.x * uv0.y);
		glm::vec3 tangent = denom * glm::vec3(
			uv1.y * edge0.x - uv0.y * edge1.x,
			uv1.y * edge0.y - uv0.y * edge1.y,
			uv1.y * edge0.z - uv0.y * edge1.z
		);
		glm::vec3 bitangent = denom * glm::vec3(
			-uv1.x * edge0.x + uv0.x * edge1.x,
			-uv1.x * edge0.y + uv0.x * edge1.y,
			-uv1.x * edge0.z + uv0.x * edge1.z
		);

		vertices[idx0].tangent = tangent;
		vertices[idx0].bitangent = bitangent;

		vertices[idx1].tangent = tangent;
		vertices[idx1].bitangent = bitangent;

		vertices[idx2].tangent = tangent;
		vertices[idx2].bitangent = bitangent;
	}

	DebugLog("Done RecalculateTangents");
}

void Mesh::UpdateBuffer() {
	if (vertices.size() <= 0)return;

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, triangles.size() * sizeof(Triangle),
		&triangles[0], GL_STATIC_DRAW);
}

int Mesh::GetIdxCount() {
	return triangles.size() * 3;
}

Mesh::Mesh(vector<Vertex> vertices_, vector<Triangle> triangles_, RenderMaterial* renderMaterial_) {
	vertices = vertices_;
	triangles = triangles_;
	renderMaterial = renderMaterial_;

	Setup();
}