#include "MeshModelLoader.h"

#include <EnvSettings.h>
#include <Render/Texture.h>
#include <Render/RenderMaterial.h>
#include <FilePooler.h>
#include <EngineDef.h>
#include <Mesh/MeshModel.h>
#include <Mesh/Mesh.h>

MeshModel* MeshModelLoader::LoadModel(string path) {
	size_t foundIdx = path.find_last_of('/');
	dirPath = path.substr(0, foundIdx + 1);
	fileName = path.substr(foundIdx + 1);

	string fullFilePath = EnvSettings::GetPathResources() + path;

	// Read file via ASSIMP
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(fullFilePath, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices);

	// Check for errors
	if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) { // if is Not Zero
		cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
		return NULL;
	}
	std::cout << "\tMesh Count: " << scene->mNumMeshes << endl;

	vector<Mesh*>* meshes = new vector<Mesh*>();
	ProcessNode(scene->mRootNode, scene, meshes);

	int vCount = 0;
	int tCount = 0;
	for (int loop = 0; loop < meshes->size(); loop++) {
		vCount += meshes->at(loop)->vertices.size();
		tCount += meshes->at(loop)->triangles.size();
	}

	std::cout << "\tVertex Count: " << vCount
		<< " / Triangle Count: " << tCount << std::endl << std::endl;

	MeshModel* retMeshModel = new MeshModel(meshes);

	dirPath = "";
	fileName = "";

	return retMeshModel;
}

/// <summary>
///
/// </summary>
/// <param name="node"></param>
/// <param name="scene"></param>
/// <param name="meshes">처리된 메쉬들을 담을 벡터</param>

void MeshModelLoader::ProcessNode(aiNode* node, const aiScene* scene, std::vector<Mesh*>* meshes) {
	// Process all the node's meshes (if any)
	for (GLuint i = 0; i < node->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		meshes->push_back(ProcessMesh(mesh, scene));
	}
	// Then do the same for each of its children
	for (GLuint i = 0; i < node->mNumChildren; i++) {
		ProcessNode(node->mChildren[i], scene, meshes);
	}
}

Mesh* MeshModelLoader::ProcessMesh(aiMesh* mesh, const aiScene* scene) {
	// Data to fill
	vector<Vertex> vertices;
	vector<Triangle> triangles;
	vector<Texture*> textures;

	// Walk through each of the mesh's vertices
	for (GLuint i = 0; i < mesh->mNumVertices; i++) {
		Vertex vertex;
		glm::vec3 vector; // We declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
						  // Positions
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		vertex.position = vector;
		// normals
		vector.x = mesh->mNormals[i].x;
		vector.y = mesh->mNormals[i].y;
		vector.z = mesh->mNormals[i].z;
		vertex.normal = vector;
		// Texture Coordinates
		if (mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.texCoords = vec;
		} else
			vertex.texCoords = glm::vec2(0.0f, 0.0f);
		vertices.push_back(vertex);
	}
	// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
	for (GLuint i = 0; i < mesh->mNumFaces; i++) {
		aiFace face = mesh->mFaces[i];
		// Retrieve all indices of the face and store them in the indices vector
		for (GLuint j = 0; j < face.mNumIndices / 3; j++) {
			Triangle tri;
			for (int loop = 0; loop < 3; loop++) {
				tri.idx[loop] = face.mIndices[j * 3 + loop];
			}

			triangles.push_back(tri);
		}
	}

	// Process materials
	RenderMaterial* renderMaterial = new RenderMaterial();
	if (mesh->mMaterialIndex >= 0) {
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

		renderMaterial->SetTexture("texture_diffuse", LoadMaterialTextures(material, aiTextureType_DIFFUSE));
		renderMaterial->SetTexture("texture_specular", LoadMaterialTextures(material, aiTextureType_SPECULAR));
		renderMaterial->SetTexture("texture_normal", LoadMaterialTextures(material, aiTextureType_NORMALS));
	}

	return new Mesh(vertices, triangles, renderMaterial);
}

Texture* MeshModelLoader::LoadMaterialTextures(aiMaterial* aiMat_, aiTextureType aiType_) {
	std::string pathResources = EnvSettings::GetPathResources();

	if (aiMat_->GetTextureCount(aiType_) > 0) {
		aiString strPath;
		aiMat_->GetTexture(aiType_, 0, &strPath);

		string fileNameTexture = string(strPath.C_Str());

		return FilePooler::LoadTexture(pathResources + dirPath + fileNameTexture);
	} else {
		return FilePooler::LoadTexture(pathResources + string("whiteone.png"));
	}
}