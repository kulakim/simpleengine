#pragma once

#include <string>
#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define AttrLoc_Position 0
#define AttrLoc_Normal 1
#define AttrLoc_TexCoord 2
#define AttrLoc_Color 3
#define AttrLoc_Tangent 4
#define AttrLoc_Bitangent 5

class Texture;
class RenderMaterial;

using namespace std;

struct Vertex {
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texCoords;
	glm::vec3 color;
	glm::vec3 tangent;
	glm::vec3 bitangent;
};

struct Triangle {
	GLuint idx[3];
};

struct FaceData {
	glm::vec3 normal;
	glm::vec3 position;
};

class Mesh {
private:
	void Setup();
	RenderMaterial* renderMaterial;

public:
	vector<Vertex> vertices;
	vector<Triangle> triangles;
	vector<FaceData> faceData;

	GLuint VAO, VBO, EBO, instanceVBO;

	Mesh();
	~Mesh();
	Mesh(vector<Vertex> vertices_, vector<Triangle> triangles_, RenderMaterial* renderMaterial_);

	void SetRenderMaterial(RenderMaterial* renderMaterial_);
	RenderMaterial* GetRenderMaterial();

	void RecalculateTangents();
	void UpdateBuffer();
	int GetIdxCount();
};
