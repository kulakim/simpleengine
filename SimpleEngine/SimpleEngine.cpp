#include "SimpleEngine.h"

#include <gl\glew.h>
#include <Bases/BasesBundle.h>
#include <GameWindow.h>

#include <assimp\postprocess.h>
#include <assimp\cimport.h>
#include <assimp\scene.h>

#include <EngineDef.h>
#include <EnvSettings.h>

#include <Imgui/imgui.h>
#include <Imgui/imgui_impl_glfw.h>
#include <Imgui/imgui_impl_opengl3.h>

bool SimpleEngine::Initialize(int width, int height, const char* name) {
	if (EnvSettings::ReadPath() == false) {
		DebugError("Failed read environment path");
		return false;
	}

	if (!glfwInit()) {
		DebugError("Failed to initialize GLFW");

		return false;
	}

	GameWindow::Init(width, height, name);

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui::StyleColorsDark();

	ImGui_ImplGlfw_InitForOpenGL(GameWindow::GetWindow(), true);
	ImGui_ImplOpenGL3_Init("#version 330");

	ImGuiIO& io = ImGui::GetIO();

	if (glewInit() != GLEW_OK) {
		DebugError("Failed to initialize GLEW");

		glfwTerminate();
		return false;
	}

	glfwSwapInterval(VSYNC_ON);

	return true;
}

bool SimpleEngine::LoadScene(Scene* scene) {
	Scene::Initialize(scene);

	Scene::GetInstance()->Load();

	return true;
}

void SimpleEngine::Begin() {
	do {
		glfwPollEvents();

		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();
		ImGui::ShowDemoWindow();

		Scene::UpdateScripts();

		Scene::RenderScene();

		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		glfwSwapBuffers(GameWindow::GetWindow());
	} while (GameWindow::ShouldClose());
}

SimpleEngine::~SimpleEngine() {
	glfwTerminate();
}