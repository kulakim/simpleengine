#pragma once

#include <gl\glew.h>
#include <vector>

#include "xstring"                            // for string
#include "glm/detail/type_mat.hpp"  // for mat4
#include "glm/detail/type_vec.hpp"     // for vec3

#define LogLength 512

#define TEXTURE_IDX_SHADOWMAP 10

using namespace std;

struct ID_matrice {
	unsigned int mvp = -1;
	unsigned int model = -1;
};

struct ID_dLight {
	unsigned int direction;
	unsigned int color;
	unsigned int power;
	unsigned int lightVP;
	unsigned int shadowMap;
};

struct ID_pLight {
	unsigned int position;
	unsigned int color;
	unsigned int power;
};

struct ID_texture {
	unsigned int diffuse;
	unsigned int normal;
	unsigned int specular;
};

class MeshRenderer;
class Texture;

//shader파일들과 매칭되는 클라스
class BaseShader {
private:
	static int currentUsingShader;

protected:
	unsigned int shaderID;
	string filePath = "NotInitialized";

public:
	BaseShader();
	BaseShader(std::string filePath_);
	virtual ~BaseShader();

	string GetFilePath();

	void Use();

	ID_matrice id_matrice;

	unsigned int GetUniformLocation(const char* var_name_);
	unsigned int GetUniformBlockIndex(const char* var_name_);

	void SetMat4(string var_name, glm::mat4 mat4_);

	void SetInt(string var_name, int val_);

	void SetFloat(string var_name, float val_);

	void SetVec2(string var_name, glm::vec2 vec2_);
	void SetVec3(string var_name, glm::vec3 vec3_);
	void SetVec4(string var_name, glm::vec4 vec4_);

	void SetMat_M(glm::mat4 matM_);
	void SetMat_MVP(glm::mat4 matMVP_);

	virtual void ApplyUniforms(MeshRenderer* renderer_) {}

	void BindLightUBO();
	void BindCameraUBO();
};
