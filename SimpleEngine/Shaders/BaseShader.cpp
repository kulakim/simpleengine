#include "BaseShader.h"
#include "ShaderBundle.h"
#include "ShaderDef.h"

#include <algorithm>
#include <Logger/SpLogger.h>
#include <EngineDef.h>
#include <fstream>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include <Util/Utils.h>
#include <Shaders/ShaderLoader.h>

#pragma region Init
int BaseShader::currentUsingShader = -1;

BaseShader::BaseShader() {
}

BaseShader::BaseShader(std::string filePath_) {
	filePath = filePath_;
	shaderID = ShaderLoader::CreateShader(filePath_);

	BindLightUBO();
	BindCameraUBO();

	id_matrice.model = GetUniformLocation("M");
	id_matrice.mvp = GetUniformLocation("MVP");

	SetInt("shadowMap", ShadowMapID);
}

void BaseShader::BindLightUBO() {
	Use();

	unsigned int lights_index = glGetUniformBlockIndex(shaderID, "LightData");
	if (lights_index != GL_INVALID_INDEX) {
		glUniformBlockBinding(shaderID, lights_index, BindingPointLightData);
	}
}

void BaseShader::BindCameraUBO() {
	Use();

	unsigned int camera_idx = glGetUniformBlockIndex(shaderID, "CameraData");
	if (camera_idx != GL_INVALID_INDEX) {
		glUniformBlockBinding(shaderID, camera_idx, BindingPointCameraData);
	}
}

#pragma endregion

BaseShader::~BaseShader() {
	glDeleteProgram(shaderID);
}

string BaseShader::GetFilePath() {
	return filePath;
}

void BaseShader::Use() {
	if (currentUsingShader != shaderID) {
		glUseProgram(shaderID);
		currentUsingShader = shaderID;
	}
}

unsigned int BaseShader::GetUniformLocation(const char* var_name) {
	return glGetUniformLocation(shaderID, var_name);
}

unsigned int BaseShader::GetUniformBlockIndex(const char* var_name) {
	return  glGetUniformBlockIndex(shaderID, var_name);
}

void BaseShader::SetMat4(string var_name, glm::mat4 mat4_) {
	Use();
	unsigned int loc = GetUniformLocation(var_name.c_str());
	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(mat4_));
}

void BaseShader::SetVec2(string var_name, glm::vec2 vec2_) {
	Use();
	unsigned int loc = GetUniformLocation(var_name.c_str());
	glUniform2f(loc, vec2_.x, vec2_.y);
}

void BaseShader::SetVec3(string var_name, glm::vec3 vec3_) {
	Use();
	unsigned int loc = GetUniformLocation(var_name.c_str());
	glUniform3f(loc, vec3_.x, vec3_.y, vec3_.z);
}

void BaseShader::SetVec4(string var_name, glm::vec4 vec4_) {
	Use();
	unsigned int loc = GetUniformLocation(var_name.c_str());
	glUniform4f(loc, vec4_.x, vec4_.y, vec4_.z, vec4_.w);
}

void BaseShader::SetInt(string var_name, int val_) {
	Use();
	unsigned int loc = GetUniformLocation(var_name.c_str());
	glUniform1i(loc, val_);
}

void BaseShader::SetFloat(string var_name, float val_) {
	Use();
	unsigned int loc = GetUniformLocation(var_name.c_str());
	glUniform1f(loc, val_);
}

void BaseShader::SetMat_M(glm::mat4 matM_) {
	Use();
	glUniformMatrix4fv(id_matrice.model, 1, GL_FALSE, glm::value_ptr(matM_));
}

void BaseShader::SetMat_MVP(glm::mat4 matMVP_) {
	Use();
	glUniformMatrix4fv(id_matrice.mvp, 1, GL_FALSE, glm::value_ptr(matMVP_));
}