﻿#include "ShaderLoader.h"
#include "ShaderDef.h"
#include <EngineDef.h>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <Util/Utils.h>
#include <gl\glew.h>
#include <vector>
#include <Logger/SpLogger.h>
#include <EnvSettings.h>
#include "BaseShader.h"

#include "ShaderBundle.h"

/// <summary>
/// 쉐이더를 생성한다.
/// </summary>
/// <param name="filePath_">쉐이더 파일 상대 경로</param>
/// <returns>생성된 ShaderID</returns>
unsigned int ShaderLoader::CreateShader(std::string filePath_) {
	DebugLog("Create Shader: " + filePath_);

	std::string pathShader = EnvSettings::GetPathShaders();

	unsigned int vShader = CreateShaderGL(
		(char*)(pathShader + filePath_ + VertexShaderFormat).c_str(),
		GL_VERTEX_SHADER
	);

	unsigned int fShader = CreateShaderGL(
		(char*)(pathShader + filePath_ + FragmentShaderFormat).c_str(),
		GL_FRAGMENT_SHADER
	);

	unsigned int gShader = CreateShaderGL(
		(char*)(pathShader + filePath_ + GeometryShaderFormat).c_str(),
		GL_GEOMETRY_SHADER
	);

	unsigned int shaderID;
	if (gShader == -1) {
		shaderID = CreateProgram(vShader, fShader);
	} else {
		shaderID = CreateProgram(vShader, gShader, fShader);
		glDeleteShader(gShader);
	}

	glDeleteShader(vShader);
	glDeleteShader(fShader);

	DebugLog("\Done CreateShader.\n");

	return shaderID;
}

/// <summary>
/// 2개의 쉐이더를 포함하는 쉐이더 프로그램을 만듭니다.
/// </summary>
/// <param name="shader0"></param>
/// <param name="shader1"></param>
unsigned int ShaderLoader::CreateProgram(unsigned int shader0_, unsigned int shader1_) {
	GLint success;
	char infoLog[LogLength];

	// Shader Program
	unsigned int shaderID = glCreateProgram();
	glAttachShader(shaderID, shader0_);
	glAttachShader(shaderID, shader1_);
	glLinkProgram(shaderID);
	// Print linking errors if any
	glGetProgramiv(shaderID, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderID, LogLength, NULL, infoLog);
		std::cout << "Error _ CreateProgram\n" << infoLog << std::endl;
	}

	return shaderID;
}

/// <summary>
/// 3개의 쉐이더를 포함하는 쉐이더 프로그램을 만듭니다.
/// </summary>
/// <param name="shader0"></param>
/// <param name="shader1"></param>
/// <param name="shader2"></param>
unsigned int ShaderLoader::CreateProgram(unsigned int shader0_, unsigned int shader1_, unsigned int shader2_) {
	GLint success;
	char infoLog[LogLength];

	// Shader Program
	unsigned int shaderID = glCreateProgram();
	glAttachShader(shaderID, shader0_);
	glAttachShader(shaderID, shader1_);
	glAttachShader(shaderID, shader2_);
	glLinkProgram(shaderID);
	// Print linking errors if any
	glGetProgramiv(shaderID, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderID, LogLength, NULL, infoLog);
		DebugError("Error CreateProgram\n" + string(infoLog));
	}

	return shaderID;
}

/// <summary>
/// glCreateShader를 통해 쉐이더를 생성한다.
/// </summary>
/// <param name="path_"></param>
/// <param name="shaderType_"></param>
/// <returns></returns>
unsigned int ShaderLoader::CreateShaderGL(char* path_, int shaderType_) {
	std::string strCode = ReadCodeFromFile(path_);
	if (strCode.size() == 0) {
		return -1;
	}

	unsigned int shader;
	shader = glCreateShader(shaderType_);
	CompileCode(shader, strCode.c_str());

	return shader;
}

std::string ShaderLoader::ReadCodeFromFile(std::string path_) {
	std::string strCode;
	std::ifstream fileStream;
	fileStream.exceptions(std::ifstream::badbit);

	try {
		fileStream.open(path_);
		std::stringstream strStream;
		strStream << fileStream.rdbuf();
		fileStream.close();
		strCode = strStream.str();
	} catch (std::ifstream::failure e) {
		DebugError("Error ReadCodeFromFile: " + path_);
	}

	strCode = ParseIncludes(strCode, path_);

	return strCode;
}

void ShaderLoader::CompileCode(int shaderId_, const char* code_) {
	GLint success;
	char infoLog[LogLength];

	glShaderSource(shaderId_, 1, &code_, NULL);
	glCompileShader(shaderId_);

	glGetShaderiv(shaderId_, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(shaderId_, LogLength, NULL, infoLog);
		DebugError("Error CompileCode: " + string(infoLog));
	}
}

/// <summary>
/// 쉐이더 내의 #include 키워드를 대응되는 파일의 코드로 치환한다.
/// </summary>
std::string ShaderLoader::ParseIncludes(std::string strCode_, std::string path_) {
	std::string retCode = "";

	std::istringstream stream(strCode_);
	std::string line;
	std::string pathShaders = EnvSettings::GetPathShaders();
	while (std::getline(stream, line)) {
		if (line.substr(0, 6).compare("//inc ") == 0) {// "//! "로 시작하는 줄?
			stringstream lineStream(line);
			std::string strTokenized;

			int tokenCount = 0;
			while (std::getline(lineStream, strTokenized, ' ')) {
				if (tokenCount == 1) {
					strTokenized.erase(std::remove(strTokenized.begin(), strTokenized.end(), '"'), strTokenized.end());
					std::string pathCode = pathShaders + strTokenized;
					//std::string pathCode = Utils::TravelPath(path_, strTokenized); for target relative path
					std::string includeCode = ReadCodeFromFile(pathCode);

					retCode += includeCode + "\n";

					break;
				}
				tokenCount++;
			}
		} else {
			retCode += line + "\n";
		}
	}

	return retCode;
}