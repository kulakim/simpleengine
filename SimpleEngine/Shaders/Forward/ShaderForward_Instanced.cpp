#include "ShaderForward_Instanced.h"
#include <Render/Texture.h>
#include <glm/gtc/type_ptr.hpp>
#include <Shaders/ShaderDef.h>
#include <Render/Renderer/MeshRenderer.h>
#include <Shaders/ShaderLoader.h>

ShaderForward_Instanced::ShaderForward_Instanced(std::string filePath_) : BaseShader(filePath_) {
	BindLightUBO();
	BindCameraUBO();

	Use();
	glUniform1i(GetUniformLocation("shadowMap"), ShadowMapID);
}

ShaderForward_Instanced::~ShaderForward_Instanced() {
}

void ShaderForward_Instanced::ApplyUniforms(MeshRenderer* renderer_) {
	Use();

	glUniformMatrix4fv(id_matrice.mvp, 1, GL_FALSE, glm::value_ptr(renderer_->MVPmatrix()));
	glUniformMatrix4fv(id_matrice.model, 1, GL_FALSE, glm::value_ptr(renderer_->Mmatrix()));
}