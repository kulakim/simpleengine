#pragma once

#include <Shaders/BaseShader.h>

class ShaderForward_Instanced : public BaseShader {
public:
	ShaderForward_Instanced(std::string path_);
	~ShaderForward_Instanced();

	virtual void ApplyUniforms(MeshRenderer* renderer_) override;
};
