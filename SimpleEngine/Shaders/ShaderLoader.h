﻿#pragma once
#include <string>

#define LogLength 512

/// <summary>
/// Shader 파일들이 Shader객체가 되는 시점까지를 담당합니다.
/// 파일 읽기, 변환, 컴파일, 객체화...
/// </summary>
static class ShaderLoader {
private:
	static unsigned int CreateProgram(unsigned int shader0_, unsigned int shader1_);
	static unsigned int CreateProgram(unsigned int shader0_, unsigned int shader1_, unsigned int shader2_);
	static unsigned int CreateShaderGL(char* path_, int shaderType_);
	static std::string ReadCodeFromFile(std::string path_);
	static void CompileCode(int shaderId_, const char* code_);
	static std::string ParseIncludes(std::string strCode_, std::string path_);

public:
	static unsigned int CreateShader(std::string filePath_);
};
