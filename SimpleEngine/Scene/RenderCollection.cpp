#include "RenderCollection.h"

#include <Render/Renderer/RendererBundle.h>
#include <Logger/SpLogger.h>

RenderCollection::RenderCollection() {
}

RenderCollection::~RenderCollection() {
}

void RenderCollection::AddRenderer(MeshRenderer* rdr_) {
	switch (rdr_->GetRenderType()) {
	case RenderType_Deferred:
		if (rdr_->IsInstanced()) {
			renderQueue_DeferredInst.push_back(rdr_);
		} else {
			renderQueue_Deferred.push_back(rdr_);
		}
		break;

	case RenderType_Forward:
		if (rdr_->IsInstanced()) {
			renderQueue_ForwardInst.push_back(rdr_);
		} else {
			renderQueue_Forward.push_back(rdr_);
		}
		break;

	default:
		DebugError("Invalid Renderer Type: " + rdr_->GetRenderType());
		break;
	}
}