#include "EngineObjectCollection.h"

#include <Bases/EngineObject.h>

EngineObjectCollection::EngineObjectCollection() {
}

EngineObjectCollection::~EngineObjectCollection() {
}

void EngineObjectCollection::AddEngineObject(EngineObject* engineObject) {
	long objectId = engineObject->GetObjectId();

	if (engineObjects.find(objectId) == engineObjects.end()) {
		engineObjects.insert(make_pair(objectId, engineObject));
	}

	rootEngineObjects.push_back(engineObject);
}

void EngineObjectCollection::RemoveEngineObject(EngineObject* engineObject) {
	engineObjects.erase(engineObject->GetObjectId());
}

void EngineObjectCollection::OnFamilyChange(EngineObject* engineObject) {
	if (engineObject->GetTransform()->GetParent() == NULL) {//부모 없음, 최상위
		rootEngineObjects[engineObject->GetObjectId()] = engineObject;
	} else {//부모 있음
		vector<EngineObject*>::iterator position =
			find(rootEngineObjects.begin(), rootEngineObjects.end(), engineObject);

		if (position != rootEngineObjects.end()) {
			rootEngineObjects.erase(position);
		}
	}
}

unordered_map<long, EngineObject*>* EngineObjectCollection::GetObjects() {
	return &(engineObjects);
}

vector<EngineObject*>* EngineObjectCollection::GetRoots() {
	return &(rootEngineObjects);;
}