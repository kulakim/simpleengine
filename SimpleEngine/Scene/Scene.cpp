#include "Scene.h"

#include "SceneIncludes.h"

#include <string>

Scene* Scene::instance;

Scene::Scene() {
	objectCollection = new EngineObjectCollection();
	renderCollection = new RenderCollection();
}

Scene::~Scene() {
	free(objectCollection);
	free(renderCollection);
}

void Scene::Initialize(Scene* scene) {
	instance = scene;

	instance->mainCamera = new Camera();
	instance->mainCamera->AttachComponent(new MoveCamera());
}

Scene* Scene::GetInstance() {
	return instance;
}

void Scene::Unload() {
	DebugLog("Unload Scene...");
}

void Scene::Load() {
	DebugLog("Load Scene...");
}

EngineObjectCollection* Scene::ObjectCollection() {
	return instance->objectCollection;
}

void Scene::AddScript(BaseScript* script_) {
	instance->scripts.push_back(script_);
	script_->OnStart();
}

void Scene::AddRenderer(MeshRenderer* rdr_) {
	instance->renderCollection->AddRenderer(rdr_);
}

Camera* Scene::MainCamera() {
	return instance->mainCamera;
}

void Scene::UpdateScripts() {
	for (int loop = 0; loop < instance->scripts.size(); loop++) {
		instance->scripts[loop]->OnUpdate();
	}
}

void Scene::RenderScene() {
	instance->mainCamera->Render(instance->renderCollection);
}