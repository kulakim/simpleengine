#pragma once

#include <string>
#include <vector>

class MeshModel;
class BaseShader;
class BaseScript;
class EngineObject;
class BaseLight;
class MeshRenderer;
class BaseRenderMaterial;
class Camera;
class RenderCollection;
class EngineObjectCollection;

using namespace std;

class Scene {
private:
	static Scene* instance;

protected:
	long freeObjectId = 0;

	Camera* mainCamera;

	EngineObjectCollection* objectCollection;

	RenderCollection* renderCollection;

	vector<BaseScript*> scripts;

public:
	//FIXME public constructor
	Scene();
	virtual ~Scene();

	static void Initialize(Scene*);

	static Camera* MainCamera();

	static Scene* GetInstance();
	static EngineObjectCollection* ObjectCollection();

	static void UpdateScripts();
	static void RenderScene();

	static void AddScript(BaseScript* upd_);
	static void AddRenderer(MeshRenderer* rdr_);

	//FIXME script ��� �� �ε�
	virtual void Unload();
	virtual void Load();
};