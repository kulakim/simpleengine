#pragma once

#include <vector>

class MeshRenderer;
class BaseLight;
class BaseRenderMaterial;
class LightManager;

// Scene -> RenderPath Param Obj
class RenderCollection {
protected:

public:
	RenderCollection();
	~RenderCollection();

	std::vector<MeshRenderer*> renderQueue_Deferred;
	std::vector<MeshRenderer*> renderQueue_DeferredInst;
	std::vector<MeshRenderer*> renderQueue_Forward;
	std::vector<MeshRenderer*> renderQueue_ForwardInst;

	void AddRenderer(MeshRenderer* rdr_);
};
