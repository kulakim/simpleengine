#pragma once

#include <SimpleEngine.h>

#include <Bases/BasesBundle.h>
#include <Light/LightsBundle.h>
#include <Mesh/MeshBundle.h>
#include <Skybox/SkyboxBundle.h>
#include <Render/Renderer/RendererBundle.h>
#include <Scripts/ScriptBundle.h>

#include <FilePooler.h>
#include <ImaginaryFigures/ImaginaryFigures.h>

#include <Render/RenderPath/RenderPathBundle.h>

#include <Logger/SpLogger.h>

#include "EngineObjectCollection.h"
#include "RenderCollection.h"