#pragma once

#include <vector>

#include <unordered_map>

class EngineObject;

using namespace std;

class EngineObjectCollection {
private:
	unordered_map<long, EngineObject*> engineObjects;

	vector<EngineObject*> rootEngineObjects;

public:
	EngineObjectCollection();
	~EngineObjectCollection();

	void AddEngineObject(EngineObject* engineObject);
	void RemoveEngineObject(EngineObject* engineObject);
	void OnFamilyChange(EngineObject* engineObject);

	unordered_map<long, EngineObject*>* GetObjects();

	vector<EngineObject*>* GetRoots();
};
