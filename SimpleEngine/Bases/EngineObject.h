#pragma once

#define strNoname "noname"

#include "Transform.h"
#include <Scene/Scene.h>

#include <vector>
#include <string>
#include <glm/gtc/matrix_transform.hpp>
#include <Logger/SpLogger.h>

class MeshRenderer;
class BaseLight;

class BaseComponent;

class EngineObject {
private:
	static long freeObjectId;
	long GetFreeObjectId();

	long objectId;

	std::vector<BaseComponent*> components;
	Transform* transform = NULL;

	void Initialize();

	bool isActive;

public:
	std::string name = strNoname;

	static void Destroy(EngineObject* engineObject_);

	EngineObject();
	~EngineObject();
	EngineObject(std::string name_);

	long GetObjectId();

	template <class T>
	T* GetComponent();
	BaseComponent* AttachComponent(BaseComponent*);
	vector<BaseComponent*> GetComponents();

	Transform* GetTransform();

	bool GetActiveState();
	void SetActiveState(bool state_);

	void OnTransformChange();
};

template<class T>
T* EngineObject::GetComponent() {
	for (int loop = 0; loop < components.size(); loop++) {
		if (T* component = dynamic_cast<T*>(components[loop])) {
			return component;
		}
	}

	return NULL;
}
