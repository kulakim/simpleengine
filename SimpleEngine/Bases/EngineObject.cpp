#include "EngineObject.h"

#include "BaseScript.h"
#include <FilePooler.h>
#include <Scene/SceneIncludes.h>

long EngineObject::freeObjectId = 0;

long EngineObject::GetFreeObjectId() {
	return ++freeObjectId;
}

void EngineObject::Initialize() {
	transform = new Transform(this);

	isActive = true;
	objectId = GetFreeObjectId();

	Scene::ObjectCollection()->AddEngineObject(this);
}

void EngineObject::Destroy(EngineObject* engineObject_) {
}

EngineObject::EngineObject() {
	Initialize();
}

EngineObject::EngineObject(std::string name_) {
	name = name_;
	Initialize();
}

long EngineObject::GetObjectId() {
	return objectId;
}

BaseComponent* EngineObject::AttachComponent(BaseComponent* baseComponent_) {
	components.push_back(baseComponent_);
	baseComponent_->OnAttachedToObject(this);

	if (BaseScript* script = dynamic_cast<BaseScript*>(baseComponent_)) {
		script->SetEngineObject(this);
		script->SetTransform(transform);
		Scene::GetInstance()->AddScript(script);

		script->OnStart();
	}

	return baseComponent_;
}

vector<BaseComponent*> EngineObject::GetComponents() {
	return components;
}

Transform* EngineObject::GetTransform() {
	return transform;
}

bool EngineObject::GetActiveState() {
	return isActive;
}

void EngineObject::SetActiveState(bool state_) {
	isActive = state_;
}

void EngineObject::OnTransformChange() {
	for (int loop = 0; loop < components.size(); loop++) {
		components[loop]->OnTransformChanged();
	}
}

EngineObject::~EngineObject() {
	free(transform);
	for (int loop = 0; loop < components.size(); loop++) {
		free(components[loop]);
	}
}