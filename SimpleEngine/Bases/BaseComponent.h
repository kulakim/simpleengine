#pragma once

class EngineObject;
class Transform;

class BaseComponent {
private:
	static long freeComponentId;

	EngineObject* engineObject;

	long componentId;
	long GetFreeComponentId();

	bool isEnabled;

protected:
	Transform* transform;

public:
	BaseComponent();
	virtual ~BaseComponent();

	virtual void DrawInspectorImGui();
	virtual void OnAttachedToObject(EngineObject* obj_);
	virtual void OnTransformChanged() {}

	EngineObject* GetEngineObject();
	void SetEngineObject(EngineObject*);

	Transform* GetTransform();
	void SetTransform(Transform*);

	long GetComponentId();

	void SetEnabled(bool enabled_);
	bool GetEnabled();
};