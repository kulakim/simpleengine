#include "Transform.h"

#include <iostream>
#include <glm/gtc/type_ptr.hpp>
#include <Bases/EngineObject.h>
#include <Scene/EngineObjectCollection.h>
#include <Util/SimpleMath.h>
#include <Imgui/imgui.h>

//현재 붙어있는 engineObject의 다른 컴포넌트들에게 변경되었음을 알림
void Transform::OnChange() {
	isMatrixDirty = true;
	engineObject->OnTransformChange();
}

void Transform::CalcModelMatrix() {
	mat4 matTranslation = translate(glm::mat4(1.0), position);
	mat4 matRotation = toMat4(rotation);
	mat4 matScale = glm::scale(glm::mat4(1.0), scale);

	modelMatrix = matTranslation * matRotation * matScale;

	isMatrixDirty = false;
}

Transform::Transform(EngineObject* engineObject_) {
	engineObject = engineObject_;

	position = glm::vec3(0, 0, 0);
	eulerAngles = glm::vec3(0, 0, 0);
	rotation = quat(eulerAngles);
	scale = glm::vec3(1, 1, 1);
	forward = glm::vec3(0, 0, 1);
	right = glm::vec3(1, 0, 0);
	up = glm::vec3(0, 1, 0);

	CalcModelMatrix();
}

vec3 Transform::GetPosition() {
	return position;
}

void Transform::SetPosition(vec3 position_) {
	position = position_;
	OnChange();
}

void Transform::AddPosition(vec3 addVec3) {
	SetPosition(position + addVec3);
}

vec3 Transform::GetScale() {
	return scale;
}

void Transform::SetScale(vec3 scale_) {
	scale = scale_;
	OnChange();
}

Transform* Transform::GetParent() {
	return parent;
}

void Transform::SetParent(Transform* parent_) {
	if (parent != NULL) {//부모가 이미 있었음. 부모에게서 자식을 지운다.
		parent->children.erase(
			std::remove(
				parent->children.begin(),
				parent->children.end(),
				this
			),
			parent->children.end()
		);
	}

	parent = parent_;

	if (parent_ != NULL) {//인수 transform의 자식으로 넣는다.
		parent_->children.push_back(this);
	}

	Scene::ObjectCollection()->OnFamilyChange(engineObject);
}

int Transform::GetChildCount() {
	return children.size();
}

Transform* Transform::GetChildAt(int idx_) {
	return children.at(idx_);
}

void Transform::AddChild(Transform* transform_) {
	children.push_back(transform_);
}

void Transform::RemoveChildAt(int idx_) {
	children.erase(children.begin() + idx_);
}

vector<Transform*>* Transform::GetChildren() {
	return &children;
}

vec3 Transform::GetEulerAngles() {
	return eulerAngles;
}

void Transform::SetEulerAngles(vec3 eulerAngles_) {
	rotation = quat(SimpleMath::DegreeToRadianV3(eulerAngles_));
}

vec3 Transform::GetForward() {
	return forward;
}

void Transform::SetForward(vec3 eulerAngles_) {
	forward = eulerAngles_;
	OnChange();
}

vec3 Transform::GetRight() {
	return right;
}

void Transform::SetRight(vec3 eulerAngles_) {
	right = eulerAngles_;
}

vec3 Transform::GetUp() {
	return up;
}

void Transform::SetUp(vec3 eulerAngles_) {
	up = eulerAngles_;
}

void Transform::Rotate(vec3 eulerAngles_) {
	rotate(rotation, eulerAngles);
}

EngineObject* Transform::GetEngineObject() {
	return engineObject;
}

void Transform::DrawInspectorImGui() {
	if (ImGui::CollapsingHeader("Transform", ImGuiTreeNodeFlags_DefaultOpen)) {
		ImGui::Text("Position");
		ImGui::SameLine(100);
		ImGui::InputFloat3("##tr_position", value_ptr(position), "%.3f", ImGuiInputTextFlags_AutoSelectAll);

		ImGui::Text("Rotation");
		ImGui::SameLine(100);
		ImGui::InputFloat3("##tr_rotation", value_ptr(eulerAngles), "%.3f", ImGuiInputTextFlags_AutoSelectAll);

		ImGui::Text("Scale");
		ImGui::SameLine(100);
		ImGui::InputFloat3("##tr_scale", value_ptr(scale), "%.3f", ImGuiInputTextFlags_AutoSelectAll);
	}
}

mat4 Transform::GetModelMatrix() {
	if (isMatrixDirty) {
		CalcModelMatrix();
	}

	return modelMatrix;
}

Transform::~Transform() {
}