#include "BaseComponent.h"
#include "EngineObject.h"

#include <Imgui/imgui.h>

long BaseComponent::freeComponentId = 0;

long BaseComponent::GetFreeComponentId() {
	return ++freeComponentId;
}

BaseComponent::BaseComponent() {
	freeComponentId = GetFreeComponentId();
}

void BaseComponent::DrawInspectorImGui() {
	ImGui::Text("Nothing to Show");
}

void BaseComponent::OnAttachedToObject(EngineObject* obj_) {
	engineObject = obj_;
	transform = engineObject->GetTransform();
}

EngineObject* BaseComponent::GetEngineObject() {
	return engineObject;
}

void BaseComponent::SetEngineObject(EngineObject* obj_) {
	engineObject = obj_;
}

Transform* BaseComponent::GetTransform() {
	return transform;
}

void BaseComponent::SetTransform(Transform* transform_) {
	transform = transform_;
}

long BaseComponent::GetComponentId() {
	return componentId;
}

BaseComponent::~BaseComponent() {
}

void BaseComponent::SetEnabled(bool enabled_) {
	isEnabled = enabled_;
}

bool BaseComponent::GetEnabled() {
	return (engineObject->GetActiveState() && isEnabled);
}