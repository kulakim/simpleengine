#pragma once

#include <string>

//path for json file describing other resource directories
#define FILENAME_SETTINGS "settings.json"

#define VAR_PATH_SHADERS "path_shaders"
#define VAR_PATH_RESOURCES "path_resources"

using namespace std;

static class EnvSettings {
private:
	static string pathShaders;
	static string pathResources;

public:
	static bool ReadPath();

	static string GetPathShaders();
	static string GetPathResources();
};
