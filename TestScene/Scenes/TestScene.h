#pragma once
#include <Scene/Scene.h>

class TestScene : public Scene {
public:
	TestScene();
	~TestScene();

	void Load() override;
	void WindowTrans();
	void NanosuitDeferred();
	void Lights();
	void Floor();
	void SphereFwd();
	void SphereDeferred();
	void SphereFwdInst();
	void Grass();
	void Rock();
};