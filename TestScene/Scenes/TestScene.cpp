#include "TestScene.h"

#include <Scene/SceneIncludes.h>

#include <string>
#include <Shaders/ShaderBundle.h>
#include <Render/RenderMaterial.h>
#include <UI/ObjectHierarchyView.h>

TestScene::TestScene() : Scene() {
}

TestScene::~TestScene() {
}

void TestScene::Load() {
	Scene::Load();

	mainCamera->SetSkybox(new SkyBox());
	mainCamera->GetTransform()->SetPosition(glm::vec3(0, 10, 20));

	EngineObject* eo = new EngineObject("HierarchyView");
	eo->AttachComponent(new ObjectHierarchyView());

	Lights();

	Floor();

	NanosuitDeferred();

	SphereFwd();

	SphereDeferred();

	SphereFwdInst();

	Grass();

	WindowTrans();

	Rock();
}

void TestScene::WindowTrans() {
	////window
	MeshModel* mQuad = new MeshModel("quad.obj");
	RenderMaterial* rmWindow = new RenderMaterial("Forward/Transparent/unlit");
	rmWindow->drawBackFace = true;
	rmWindow->isTransparent = true;
	rmWindow->SetTexture("texture_diffuse", FilePooler::LoadTexture("window.png"));
	rmWindow->SetVec4("color", glm::vec4(1, 1, 1, 0.5f));

	mQuad->GetMesh(0)->SetRenderMaterial(rmWindow);

	EngineObject* eo = new EngineObject("window");
	MeshRenderer* rdrWindow = new MeshRenderer(RenderType_Forward);
	rdrWindow->SetMeshModel(mQuad);

	eo->GetTransform()->SetEulerAngles(glm::vec3(90, 0, 0));
	eo->GetTransform()->SetPosition(glm::vec3(-5, 5, -5));
	eo->AttachComponent(rdrWindow);
}

void TestScene::NanosuitDeferred() {
	EngineObject* eoNano = new EngineObject("nano");
	MeshRenderer* rdrNano = new MeshRenderer(RenderType_Deferred);
	rdrNano->SetMeshModel(FilePooler::LoadMeshModel("nanosuit/nanosuit.obj"));
	eoNano->AttachComponent(rdrNano);
	eoNano->GetTransform()->SetPosition(glm::vec3(0, 0, -10));
}

void TestScene::Lights() {
	EngineObject* eoLight;

	eoLight = new EngineObject("light");
	DirectionalLight* directionalLight = new DirectionalLight();
	eoLight->AttachComponent(directionalLight);
	eoLight->GetTransform()->SetForward(glm::vec3(0.7, -0.7, 0));

	PointLight* pointLight = new PointLight();
	pointLight->SetColor(glm::vec3(1, 0, 0));
	pointLight->SetRange(10);
	eoLight = new EngineObject("pLight");
	eoLight->AttachComponent(pointLight);
	eoLight->GetTransform()->SetPosition(vec3(30.0f, 5.0f, -10.0f));
}

void TestScene::Floor() {
	MeshModel* mPlane = FilePooler::LoadMeshModel("plane.obj");
	RenderMaterial* rmForwardSand = new RenderMaterial("Forward/opaque");
	rmForwardSand->SetTexture("texture_diffuse", FilePooler::LoadTexture("burnt_sand_brown.png"));

	mPlane->GetMesh(0)->SetRenderMaterial(rmForwardSand);
	for (int loop = 0; loop < mPlane->GetMesh(0)->vertices.size(); loop++) {
		mPlane->GetMesh(0)->vertices[loop].texCoords =
			glm::vec2(
				mPlane->GetMesh(0)->vertices[loop].position.x,
				mPlane->GetMesh(0)->vertices[loop].position.z
			);
	}
	mPlane->GetMesh(0)->UpdateBuffer();
	EngineObject* eoFloor = new EngineObject("floor");
	MeshRenderer* rdrFloorForward = new MeshRenderer(RenderType_Forward);
	rdrFloorForward->SetMeshModel(mPlane);

	eoFloor->GetTransform()->SetScale(glm::vec3(40, 1, 40));
	eoFloor->AttachComponent(rdrFloorForward);
}

void TestScene::SphereFwd() {
	EngineObject* eoSphFw = new EngineObject("sphere_fwd");
	RenderMaterial* rmSphereForward = new RenderMaterial("Forward/opaque");
	rmSphereForward->SetTexture("texture_diffuse", FilePooler::LoadTexture("whiteone.png"));
	MeshRenderer* rdrSphFw = new MeshRenderer(RenderType_Forward);
	rdrSphFw->SetMeshModel(new MeshModel("sphere.obj"));
	rdrSphFw->GetMesh(0)->SetRenderMaterial(rmSphereForward);
	eoSphFw->AttachComponent(rdrSphFw);
	eoSphFw->GetTransform()->SetPosition(glm::vec3(-5, 2, 0));
}

void TestScene::SphereDeferred() {
	EngineObject* eoSphDf = new EngineObject("sphere_df");
	MeshRenderer* rdrSphDf = new MeshRenderer(RenderType_Deferred);
	RenderMaterial* rmSphereDeferred = new RenderMaterial();
	rmSphereDeferred->SetTexture("texture_diffuse", FilePooler::LoadTexture("burnt_sand_brown.png"));
	rdrSphDf->SetMeshModel(new MeshModel("sphere.obj"));
	rdrSphDf->GetMesh(0)->SetRenderMaterial(rmSphereDeferred);
	eoSphDf->AttachComponent(rdrSphDf);
	eoSphDf->GetTransform()->SetPosition(glm::vec3(-8, 2, 0));
}

void TestScene::SphereFwdInst() {
	EngineObject* eoInst = new EngineObject("sphere Inst");
	MeshRendererInstanced* rdrInst = new MeshRendererInstanced(RenderType_Forward);
	RenderMaterial* rmSphereForwardInst = new RenderMaterial("Forward/Instanced/opaque");
	rmSphereForwardInst->drawBackFace = false;

	rmSphereForwardInst->SetTexture("texture_diffuse", FilePooler::LoadTexture("whiteone.png"));
	rdrInst->SetMeshModel(FilePooler::LoadMeshModel("Sphere/sphere_64_32.obj"));
	rdrInst->GetMesh(0)->SetRenderMaterial(rmSphereForwardInst);
	eoInst->AttachComponent(rdrInst);
	eoInst->GetTransform()->SetPosition(glm::vec3());
	int sphereCount = 10;
	for (int loop = 0; loop < sphereCount; loop++) {
		for (int loop2 = 0; loop2 < sphereCount; loop2++) {
			EngineObject* eoChild = new EngineObject("sphere");
			eoChild->GetTransform()->SetParent(eoInst->GetTransform());
			eoChild->GetTransform()->SetPosition(glm::vec3(loop * 3 + 20, 2, loop2 * 3 - 20));
		}
	}
	rdrInst->InitInstanced();
}

void TestScene::Grass() {
	EngineObject* eoGrassParent = new EngineObject("grass parent");
	MeshRendererInstanced* rdrInstGrass = new MeshRendererInstanced(RenderType_Forward);
	RenderMaterial* rmGrassFwdInst = new RenderMaterial("Forward/Transparent/Instanced/cutout");
	rmGrassFwdInst->drawBackFace = true;
	rmGrassFwdInst->SetTexture("texture_diffuse", new Texture("grass.png", true));
	rmGrassFwdInst->SetFloat("alphaCutout", 0.5f);

	MeshModel* quadMm = new MeshModel("quad.obj");
	rdrInstGrass->SetMeshModel(quadMm);

	rdrInstGrass->GetMesh(0)->SetRenderMaterial(rmGrassFwdInst);
	eoGrassParent->AttachComponent(rdrInstGrass);
	eoGrassParent->GetTransform()->SetPosition(glm::vec3());
	int grassCount = 100;
	for (int loop = 0; loop < grassCount; loop++) {
		for (int loop2 = 0; loop2 < grassCount; loop2++) {
			EngineObject* eoChild = new EngineObject("grass");
			eoChild->GetTransform()->SetParent(eoGrassParent->GetTransform());
			eoChild->GetTransform()->SetScale(glm::vec3(rand() % 2 + 2, 2, rand() % 2 + 2));
			eoChild->GetTransform()->SetPosition(glm::vec3(-rand() % 100 - 20, 2, -rand() % 100 + 50));
			eoChild->GetTransform()->SetEulerAngles(glm::vec3(90, rand() % 180, 0));
		}
	}

	rdrInstGrass->InitInstanced();
}

void TestScene::Rock() {
	MeshModel* meshModel = new MeshModel("Rock/Rock.fbx");
	RenderMaterial* renderMat = new RenderMaterial("Forward/bumpedDiffuse");
	renderMat->SetTexture("texture_diffuse", FilePooler::LoadTexture("Rock/Rock_d.jpg"));
	renderMat->SetTexture("texture_normal", FilePooler::LoadTexture("Rock/Rock_n.jpg"));
	renderMat->SetFloat("shininess", 2.0f);

	meshModel->GetMesh(0)->SetRenderMaterial(renderMat);

	EngineObject* eo = new EngineObject("Rock");
	MeshRenderer* rdr = new MeshRenderer(RenderType_Forward);
	rdr->SetMeshModel(meshModel);

	eo->GetTransform()->SetScale(glm::vec3(0.02f, 0.02f, 0.02f));
	eo->GetTransform()->SetPosition(glm::vec3(-10, 8, -15));
	eo->AttachComponent(rdr);
}