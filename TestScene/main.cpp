#include "SimpleEngine.h"
#include "Scenes/TestScene.h"

int main() {
	if (SimpleEngine::Initialize(1536, 864, "Test Scene") == false) {
		return 0;
	}

	SimpleEngine::LoadScene(new TestScene());
	SimpleEngine::Begin();

	return 0;
}