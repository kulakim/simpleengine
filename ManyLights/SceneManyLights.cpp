#include "SceneManyLights.h"

#include <Scene/SceneIncludes.h>
#include <Render/RenderMaterial.h>

void SceneManyLights::Load() {
	Scene::Load();

	LightManager::Inst()->SetAmbient(glm::vec3(0.1f, 0.1f, 0.1f));

	EngineObject* eoInst = new EngineObject("sphere Inst");
	MeshRendererInstanced* rdrInst = new MeshRendererInstanced(RenderType_Deferred);
	RenderMaterial* rmSphereForwardInst = new RenderMaterial();

	rmSphereForwardInst->SetTexture("texture_diffuse", FilePooler::LoadTexture("whiteone.png"));
	rdrInst->SetMeshModel(FilePooler::LoadMeshModel("Sphere/sphere_32_16.obj"));
	rdrInst->GetMesh(0)->SetRenderMaterial(rmSphereForwardInst);
	eoInst->AttachComponent(rdrInst);
	eoInst->GetTransform()->SetPosition(glm::vec3());
	int sphereCount = 100;
	for (int loop = 0; loop < sphereCount; loop++) {
		for (int loop2 = 0; loop2 < sphereCount; loop2++) {
			EngineObject* eoChild = new EngineObject("sphere");
			Transform* trChild = eoChild->GetTransform();
			trChild->SetParent(eoInst->GetTransform());
			trChild->SetPosition(glm::vec3(rand() % 200 - 100, rand() % 200 - 100, rand() % 200 - 100));
			float targetScale = rand() % 2 + 2;
			trChild->SetScale(glm::vec3(targetScale));
		}
	}
	rdrInst->InitInstanced();

	InstantiateLights();
}

void SceneManyLights::InstantiateLights() {
	PointLight* pointLight;
	EngineObject* eoLight;

	int lightCount = 1000;

	for (int loop = 0; loop < lightCount; loop++) {
		int rand_range = rand() % 30 + 5;
		float rand_r = (rand() % 128 + 128) * 0.004f;
		float rand_g = (rand() % 128 + 128) * 0.004f;
		float rand_b = (rand() % 128 + 128) * 0.004f;

		pointLight = new PointLight();
		pointLight->SetRange(rand_range);
		pointLight->SetColor(vec3(rand_r, rand_g, rand_b));
		pointLight->castShadow = false;

		eoLight = new EngineObject("pLight");
		eoLight->AttachComponent(pointLight);
		eoLight->GetTransform()->SetPosition(vec3(rand() % 220 - 110, rand() % 220 - 110, rand() % 220 - 110));
	}
}