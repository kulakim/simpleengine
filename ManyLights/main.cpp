#include "SimpleEngine.h"
#include "SceneManyLights.h"

int main() {
	if (SimpleEngine::Initialize(1536, 864, "Many Lights") == false) {
		return 0;
	}

	SimpleEngine::LoadScene(new SceneManyLights());
	SimpleEngine::Begin();

	return 0;
}